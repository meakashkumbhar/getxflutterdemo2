package com.example.demogetx

import io.flutter.embedding.android.FlutterActivity
import com.microsoft.appcenter.AppCenter;
import com.microsoft.appcenter.analytics.Analytics;
import com.microsoft.appcenter.crashes.Crashes;

class MainActivity: FlutterActivity() {
    override fun onCreate(){
        AppCenter.start(getApplication(), "8982a3f4-18b8-4c1d-8222-c6e89be8ccc8",
                  Analytics.class, Crashes.class);
    }

}
