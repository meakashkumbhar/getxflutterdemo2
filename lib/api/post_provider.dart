import 'package:get/get.dart';

class PostProvider extends GetConnect {
  Future<Response> getPosts() =>
      get('https://jsonplaceholder.typicode.com/posts');

  Future<Response> sendPostData(Map data) =>
      post('https://jsonplaceholder.typicode.com/posts', data,
          contentType: 'application/json; charset=UTF-8');
  Future<Response> sendPostData2(Map data) =>
      post('https://jsonplaceholder.typicode.com/posts', data,
          contentType: 'application/json; charset=UTF-8');
  Future<Response> sendPostData3(Map data) =>
      post('https://jsonplaceholder.typicode.com/posts', data,
          contentType: 'application/json; charset=UTF-8');
}
